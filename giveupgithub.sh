#!/bin/bash
# (c) 2022 Brett Sheffield
# (c) 2023 Thijs Paelman
# LICENSE: GPL3

# account on github
SRCACCT=librecast
# account on destination forge
DSTACCT=librecast
# branch on github
BRANCH=main
# project website
PROJECTWEB="https://librecast.net/"

if [ "$#" -ne 1 ]; then
	exit 1
fi

mkdir "$1"
sed -e 's/DSTACCT/'"$DSTACCT"'/g' < README.template.md |\
sed -e 's%PROJECTWEBSITE%'"$PROJECTWEB"'%g' |\
sed -e 's/PPPROJECT/'"$1"'/g' > "$1"/README.md
cd "$1"
git init
git add README.md
git commit README.md --message="Give Up Github"
git remote add github git@github.com:"$SRCACCT"/"$1".git

# Remove all remote tags & branches
# Will give an error for the main branch: `! [remote rejected]`, just ignore it
git push --delete github $(git ls-remote --refs github | cut -f2 | tr '\n' ' ')
git push --set-upstream github "HEAD:$BRANCH" --force
cd ..
